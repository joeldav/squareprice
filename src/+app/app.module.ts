import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DocsModule } from './+documentation/docs.module';
import { HomeModule } from './+home/home.module';
import { AboutModule } from './+about/about.module';
import { SharedModule } from './shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent, XLargeDirective } from './app.component';
import { startLogging } from './logger';

@NgModule({
  declarations: [ AppComponent, XLargeDirective ],
  imports: [
    SharedModule,
    HomeModule,
    DocsModule,
    AboutModule,
    AppRoutingModule
  ],
  providers: []
})
export class AppModule {

  constructor() {
    //startLogging();
  }
}

export { AppComponent } from './app.component';
