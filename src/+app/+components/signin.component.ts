import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector:'signin',
    template:`
        <div [formGroup]="parent">
        <div formGroupName="signin">
            <input
                    type="text"
                    formControlName="username"
                    placeholder="Username">
            <input
                    type="text"
                    formControlName="password"
                    placeholder="Password">
        </div>
        </div>
    `
})
export class SignInComponent {
    @Input()
    parent:FormGroup;
}