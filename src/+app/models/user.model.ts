import * as mongoose from 'mongoose';
import { IUser } from '../interfaces/user.interface';

var userSchema = new mongoose.Schema({
  name: {type:String, required:true},
  email: {type:String, required:true},
  pkgId: mongoose.Schema.Types.ObjectId,
  apiKey: {type:String, required:true},
  config: {type:Object},
  registered: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now },
  retailers:{type:mongoose.Schema.Types.Mixed,required:true}
});
userSchema.index({ email: 1,apiKey: 1 }, { unique: true });

type UserType = IUser & mongoose.Document;
var _model = mongoose.model <UserType> ('User', userSchema);

export class User {

  static getAll(): Promise<IUser> {
    return new Promise<IUser> ((resolve, reject) => {
      _model.find((err, users) => {
        err ? reject(err) : resolve(users);
      });
    });
  }
  static getOne(searchParams): Promise<IUser> {
    return new Promise<IUser> ((resolve, reject) => {
      _model.findOne(searchParams,(err, users) => {
        err ? reject(err) : resolve(users);
      });
    });
  }

}
