import {Injectable} from "@angular/core";
import { observable, computed, action } from 'mobx';

@Injectable()
export class TestService {

  @observable completed:boolean = false;
  @action toggle() {

    this.completed = !this.completed;
  }

}
