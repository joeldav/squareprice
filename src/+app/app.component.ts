import { Component, Directive, ElementRef, Renderer, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
//
/////////////////////////
// ** Example Directive
// Notice we don't touch the Element directly

@Directive({
  selector: '[xLarge]'
})
export class XLargeDirective {
  constructor(element: ElementRef, renderer: Renderer) {
    // ** IMPORTANT **
    // we must interact with the dom through -Renderer-
    // for webworker/server to see the changes
    renderer.setElementStyle(element.nativeElement, 'fontSize', 'x-large');
    // ^^
  }
}

@Component({
  changeDetection: ChangeDetectionStrategy.Default,
  encapsulation: ViewEncapsulation.Emulated,
  selector: 'app',
  styleUrls: [ '../assets/css/main.css' ],
  template: `
  <section class="header">
  <span class="logo-container"><span class="logo">SQUAREPRICE</span></span>
  <nav>
    <a routerLinkActive="router-link-active" routerLink="home">Features</a>
    <a routerLinkActive="router-link-active" routerLink="documentation">Documentation</a>
    <a routerLinkActive="router-link-active" routerLink="#">Sign In</a>
    <a routerLinkActive="router-link-active" routerLink="#" class="register">Get started for free</a>
    <a routerLinkActive="router-link-active" routerLink="contact">Contact</a>
  </nav>
  </section>
  <div class="hero-universal">
    <div class="inner-hero">
      <main>
        <router-outlet></router-outlet>
      </main>
    </div>
  </div>
  <footer>
  <p>&copy; SquarePrice 2017</p>
</footer>
  `
})
export class AppComponent {
  title = 'ftw';
}
