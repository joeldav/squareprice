import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { DocsComponent } from './docs.component';
import { DocsRoutingModule } from './docs-routing.module';

@NgModule({
  imports: [
    SharedModule,
    DocsRoutingModule
  ],
  declarations: [
    DocsComponent
  ]
})
export class DocsModule { }
