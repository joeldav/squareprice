import { Component, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';

import { ModelService } from '../shared/model/model.service';

@Component({
  changeDetection: ChangeDetectionStrategy.Default,
  encapsulation: ViewEncapsulation.Emulated,
  selector: 'docs',
  styleUrls: [ './docs.component.css' ],
  templateUrl: './docs.component.html'
})
export class DocsComponent {
  data: any = {};
  constructor(public model: ModelService) {

    // we need the data synchronously for the client to set the server response
    // we create another method so we have more control for testing
    this.universalInit();
  }

  universalInit() {
   // this.model.get('/data.json').subscribe(data => {
     // this.data = data;
   // });
  }

}
