export interface IUser {
        name: string,
        email: string,
        pkgId: number,
        apiKey: string,
        config: Array<any>,
        registered: Date,
        updated: Date,
        retailers:Array<any>
}
