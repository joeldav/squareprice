import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { LazyComponent } from './lazy.component';
import { LazyRoutingModule } from './lazy-routing.module';
import {TestService} from "../rxstore";
import { Ng2MobxModule } from 'ng2-mobx';

@NgModule({
  imports: [
    SharedModule,
    LazyRoutingModule,
    Ng2MobxModule,
  ],
  declarations: [
    LazyComponent
  ],
  providers:[
    TestService
  ]
})
export class LazyModule { }
