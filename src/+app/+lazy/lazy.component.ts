import { Component, Inject, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { TestService } from '../rxstore';
import {Http, Headers, RequestOptions} from "@angular/http";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.Emulated,
  selector: 'lazy',
  template: `Lazy compon ent  <div id="text">myTextsss:  s{{myText}}</div><div *mobxAutorun>
      {{ store.completed }}
      <div (click)="api()">test</div>
      <div (click)="setup()">SETUP</div>
      <input class="toggle" type="checkbox" [checked]="store.completed" (change)="store.toggle()">
    </div>`
})
export class LazyComponent {

  myText = 'test';
  store;
  private headers = new Headers({ 'Content-Type': 'application/json', 'charset': 'UTF-8' });
  private options = new RequestOptions({ headers: this.headers });

  constructor(testService:TestService,public http:Http) {
      this.store = testService;
  }

  api() {
    console.log('test');
    /*this.http.post('/api/user', {name:'test',email:'joe@dd.com',age:24},this.options)
      .map(res => res.json())
      .subscribe();*/
    this.http.post('/api/price', {price:24,priceMatch:100,date:new Date(),userId:'5873ff494932df98a542de29'},this.options)
        .map(res => res.json())
        .subscribe();
    this.http.get('/api/users')
      .map(res => res.json())
      .subscribe();
  }
  setup() {
    //starter
    this.http.post('/setup', {
      name:'Starter',
      crawls:10,
      history_months:1,
      api:true,
      csv:false,

    },this.options)
        .map(res => res.json())
        .subscribe();

    //basic
    this.http.post('/setup', {
      name:'Basic',
      crawls:500,
      history_months:6,
      api:true,
      csv:false,

    },this.options)
        .map(res => res.json())
        .subscribe();

    //pro
    this.http.post('/setup', {
      name:'Pro',
      crawls:2000,
      history_months:12,
      api:true,
      csv:false,

    },this.options)
        .map(res => res.json())
        .subscribe();

    setTimeout(() => {
      this.http.post('/setup', {
        name:'Pro',
        crawls:2000,
        history_months:12,
        api:true,
        csv:false,

      },this.options)
          .map(res => res.json())
          .subscribe();
    },3000);
  }





}
