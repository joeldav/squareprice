import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { LoginComponent } from './login.component';
import { LoginRoutingModule } from './login-routing.module';
import {TestService} from "../rxstore";
import { Ng2MobxModule } from 'ng2-mobx';
import { RegisterComponent } from '../+components/register.component';
import { SignInComponent } from '../+components/signin.component';
import { HttpModule } from '@angular/http';
import { LoginService } from './+services/login.service';

@NgModule({
  imports: [
    SharedModule,
    LoginRoutingModule,
    Ng2MobxModule,
    HttpModule
  ],
  declarations: [
    LoginComponent,
    SignInComponent,
    RegisterComponent
  ],
  providers:[
    LoginService
  ]
})
export class LoginModule { }
