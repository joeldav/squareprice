import { Component, Inject, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { TestService } from '../rxstore';
import {Http, Headers, RequestOptions} from "@angular/http";
import {FormBuilder, FormGroup, FormArray} from  "@angular/forms";
import { LoginService } from './+services/login.service';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.Emulated,
  selector: 'login',
  templateUrl: 'login.html'
})
export class LoginComponent {

  form = this.fb.group({
    signin: this.fb.group({
      username: '',
      password: ''
    }),
    register:this.fb.group({
      password: '',
      email: ''
    })
  });

  constructor(private fb:FormBuilder, private ls:LoginService) {


  }

  onSubmit() {
    this.ls.loginUser(this.form.value.signin);
    console.log("Submit:", this.form.value);
  }





}
