import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

export function getLazyModule() {
  return System.import('./+lazy/lazy.module' + (process.env.AOT ? '.ngfactory' : ''))
    .then(mod => mod[(process.env.AOT ? 'LazyModuleNgFactory' : 'LazyModule')]);
}

export function getUsersModule() {
  return System.import('./+login/login.module' + (process.env.AOT ? '.ngfactory' : ''))
      .then(mod => mod[(process.env.AOT ? 'LoginModuleNgFactory' : 'LoginModule')]);
}

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', redirectTo: '/home', pathMatch: 'full' },
      { path: 'lazy', loadChildren: getLazyModule },
      { path: 'users/sign_in', loadChildren: getUsersModule }
    ])
  ],
})
export class AppRoutingModule { }
