// the polyfills must be one of the first things imported in node.js.
// The only modules to be imported higher - node modules with es6-promise 3.x or other Promise polyfill dependency
// (rule of thumb: do it if you have zone.js exception that it has been overwritten)
// if you are including modules that modify Promise, such as NewRelic,, you must include them before polyfills
import 'angular2-universal-polyfills';
import 'ts-helpers';
import './__workaround.node'; // temporary until 2.1.1 things are patched in Core


import * as path from 'path';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';
import * as morgan from 'morgan';
import * as compression from 'compression';
import {Responses} from './backend/responses';

//import * as jobs from "cron";

var config = require('./config');
var jwt    = require('jsonwebtoken');
var mongoose = require('mongoose');
var RateLimit = require('express-rate-limit');


//setup models

import {User} from '../src/+app/models/user.model';
var Packages = require('../models/package.model.js');
var Crawl = require('../models/crawl.model.js');
var Servers = require('../models/servers.model.js');

//start scheduler
//jobs();

// Angular 2
import { enableProdMode } from '@angular/core';
// Angular 2 Universal
import { createEngine } from 'angular2-express-engine';

// App
import { MainModule } from './node.module';

// Routes
import { routes } from './server.routes';

// enable prod for faster renders
enableProdMode();

export const app = express();
const ROOT = path.join(path.resolve(__dirname, '..'));

//api limits

// Express View
app.engine('.html', createEngine({
  ngModule: MainModule,
  providers: [
    // use only if you have shared state between users
    // { provide: 'LRU', useFactory: () => new LRU(10) }

    // stateless providers only since it's shared
  ]
}));
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname);
app.set('view engine', 'html');
app.set('json spaces', 2);
app.set('superSecret', config.secret); // secret variable

app.use(cookieParser('Angular 2 Universal'));
app.use(bodyParser.json());
app.use(compression());

app.use(morgan('dev'));

mongoose.connect('mongodb://localhost:27017/squareprice', { config: { autoIndex: false } });
var db = mongoose.connection;
mongoose.Promise = global.Promise;

//mongoDB connection
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('Connected to MongoDB');
});

// APIs
var authenticateLimiter = new RateLimit({
  windowMs: 60*60*1000, // 1 hour window
  delayAfter: 1, // begin slowing down responses after the first request
  delayMs: 2*1000, // slow down subsequent responses by 3 seconds per request
  max: 5, // start blocking after 5k requests
  message: "Too many request from this IP, please try again after an hour"
});


// create
app.post('/api/authenticate',authenticateLimiter, function(req, res) {
  // find the user
  if(!req.headers['x-sp-key']) {
    return new Responses(res,"NO_TOKEN");
  }
  User.getOne({
    apiKey: req.headers['x-sp-key']
  }).then((result) => {

    if (!result) {
      //res.json({ success: false, message: 'Authentication failed. User not found.' });
      return new Responses(res,"NO_USER");
    } else if (result) {

      // check if password matches
      if (result.apiKey != req.headers['x-sp-key']) {
        return new Responses(res,"INVALID_API_KEY");
      } else {

        // if user is found and password is right
        // create a token
        var token = jwt.sign(result, app.get('superSecret'), {
          expiresIn: "24h" // expires in 24 hours
        });

        // return the information including token as JSON
        res.json({
          success: true,
          expires: new Date(new Date().getTime() + 60 * 60 * 24 * 1000),
          token: token
        });
      }

    }

  });
});

function cacheControl(req, res, next) {
  // instruct browser to revalidate in 60 seconds
  res.header('Cache-Control', 'max-age=60');
  next();
}
// Serve static files
app.use('/assets', cacheControl, express.static(path.join(__dirname, 'assets'), {maxAge: 30}));
app.use(cacheControl, express.static(path.join(ROOT, 'dist/client'), {index: false}));

//frontend
app.post('/setup', function(req, res) {
    var obj = new Packages(req.body);
    obj.save(function(err, obj) {
      if(err) return console.error(err);
      res.status(200).json(obj);
    });
});

//
/////////////////////////
// ** Example API
// Notice API should be in aseparate process
import { createCrawlApi } from './backend/api';



// Our API
app.use('/api', createCrawlApi());

function ngApp(req, res) {
  res.render('index', {
    req,
    res,
    // time: true, // use this to determine what part of your app is slow only in development
    preboot: false,
    baseUrl: '/',
    requestUrl: req.originalUrl,
    originUrl: `http://localhost:${ app.get('port') }`
  });
}
/**
 * use universal for specific routes
 */
app.get('/', ngApp);
routes.forEach(route => {
  app.get(`/${route}`, ngApp);
  app.get(`/${route}/*`, ngApp);
});

app.get('*', function(req, res) {
  res.setHeader('Content-Type', 'application/json');
  var pojo = { status: 404, message: 'No Content' };
  var json = JSON.stringify(pojo, null, 2);
  res.status(404).send(json);
});

// Server
let server = app.listen(app.get('port'), () => {
  console.log(`Listening on: http://localhost:${server.address().port}`);
});
