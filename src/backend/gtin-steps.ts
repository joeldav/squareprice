'use strict';

var cheerio = require('cheerio');
export class gtinSteps {

    link: string;
    qlink: string;
    steps = [
        {
            valid: () => {
                return this.valid()
            },
            url: () => {
                return this.ip + '/g.php?barcode=' + this.data.identifier;
            },
            search: (results) => {
                //console.log("RESULTS", results);
                return new Promise((resolve, reject) => {
                    let json = JSON.parse(results);
                    if (json.err) {
                        reject(json);
                    }
                    //console.log("JSON",json.part1);
                    let $ = cheerio.load(json.html);
                    //console.log($.html());
                    let brands = [];
                    let fullData = {
                        allBrands:[],
                        html:json
                    };
                    var _self = this;
                    let a = $('table#os-sellers-table tr').each(function (i, elem) {
                        //6 columns
                        let price, brand;
                        $(this).find('td').each(function (j, elem) {
                            if (j == 0) brand = $(this).text().replace(/ /g, '');
                            if (j == 4) price = parseFloat($(this).text().match(/[+\-]?\d+(,\d+)?(\.\d+)?/)[0].replace(",", "")).toLocaleString('en-US', {minimumFractionDigits: 2});

                        });



                        if (brand != null && price != null && price != 'NaN' && _self.config.brands.indexOf(brand) <= -1) {
                            //search for wildcards <searchTerm>
                            var allowed = true;
                            _self.config.brands.forEach(function(element) {
                                if(element.indexOf('>') > 0) {
                                    var searchStr = element.split('<').pop().split('>').shift();
                                    if (brand.toLowerCase().search(searchStr) < 0) {

                                    } else {
                                        allowed = false;
                                    }
                                }
                            });
                            if(allowed) {
                                brands.push({
                                    brand: brand,
                                    price: price
                                });
                            }
                        }
                        if (brand != null && price != null && price != 'NaN') {
                            fullData.allBrands.push({
                                brand: brand,
                                price: price
                            });
                        }
                        brand,price = null;

                    });
                    let data = {
                        result:this.validate(brands),
                        full:fullData
                    };
                    json,brands = null;
                    resolve(data);
                });
            }
        }
    ];

    constructor(
        private config: any,
        private data: any,
        private ip: any
    ) {

    }

    valid() {
        return new Promise((resolve,reject) => {
            let eanCode = this.data.identifier;
            eanCode = eanCode.trim();
            if (eanCode.length < 13) {
                eanCode = "0"+eanCode; //add 0's as padding
            }
            if (eanCode.length < 14) {
                eanCode = "0"+eanCode; //add 0's as padding
            }
            if ([8, 12, 13, 14].indexOf(eanCode.length) == -1) {
                reject('INVALID_BARCODE_LENGTH');
            }
            this.data.identifier = eanCode;
            resolve();

        });

    }

    validate(brands) {
        let match = this.getMatch(brands);
        let obj = {
            status: 400,
            price: null,
            brand: null
        };
        if (!match) {
            obj.status = 204;
            obj.price = null;
        }
        else if (brands.length > 0 && !match) {
            obj.status = 206;
            obj.price = null;
        } else {
            obj.status = 200;
            obj.price = match.price;
            obj.brand = match.brand;
        }
        return obj;

    }

    getMatch(data: any) {
        //console.log(data);
        let prices = [];
        for (var i = 0; i < data.length; i++) {
            prices.push({price:parseFloat(data[i].price).toFixed(2),brand:data[i].brand});
        }
        //console.log(prices);
        if (prices.length > 0) {
            //prices.reduce(function (a, b, i, prices) {
                // price = Math.min(a, b)
            //});
            prices.sort(function(a, b) {
                a = a.price;
                b = b.price;
                if(a < b)
                    return -1;
                if(a > b)
                    return 1;
                return 0;
            });
            return prices[0];

        } else {
            return false;
        }
    }

    getBrand(text: string) {
        //console.log("got:"+text);
        let result = text.match(/[A-Za-z0-9.]+/)[0];
        //console.log("returnung:"+result);
        return (!result) ? null : result;
    }

    getPrice(text: string) {
        let result = text.match(/[A-Za-z0-9.]+/)[0];
        return (!result) ? null : result;
    }
    check() {
        return (this.data.identifier);
    }

    setLink(link) {
        this.link = link;
    }

    setQLink(link) {
        this.qlink = link;
    }

    getQLink() {
        return this.qlink;
    }

    getLink() {
        return this.link;
    }

    getSteps() {
        return this.steps;
    }

}