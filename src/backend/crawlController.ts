import {Step} from "./step";
import {Crawler} from "./crawl";
import {forEach} from "@angular/router/src/utils/collection";

export class CrawlController {
    results:Array<Object>;

    constructor(private steps:any) {
    }

    processStep(step:Step) {
        return new Promise((resolve,reject) => {
                let options = {
                    url:step.url(),
                    verbose: true,
                    stderr: true,
                    timeout:28,
                    'connect-timeout':28,
                    'max-time':28,

                };
                new Crawler().getPage(options).then((results) => {
                    step.search(results).then((res) => {
                        resolve(res);
                    }).catch((err) => {
                        reject(err);
                    })
                }).catch((err) => {
                    reject(err);
                });

        });

    }
    runSteps() {
        let promise = Promise.resolve();
        var _self = this;
            for(var i=0; i < this.steps.length; i++) {
                (function (i) {
                    promise = promise.then((res) => {
                        return _self.processStep(_self.steps[i]);
                    });
                })(i);

            }

        promise = promise.then((res) => {
            return res;
        }).catch((err) => {
            return Promise.reject(err);
        });
        return promise;


    }
}