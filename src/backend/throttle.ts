import {Responses} from './responses';
var Throttle = require('../../models/user/throttle.model.js');
var Packages = require('../../models/package.model.js');


export class Throttler {

    constructor(public req,public res,public next) {
        this.throttle(req,res,next);
    }

    userPkg(id, next) {
        Packages.findOne({_id: id}, function (err, pkgs) {
            if (err) {
                //console.log('err', err);
            } else {
                next(pkgs)
            }
        })
    }

    respondWithThrottle(request, response, next, throttle, pkg) {
        var timeUntilReset = pkg.seconds -
                (new Date().getTime() - throttle.createdAt.getTime()),
            remaining = Math.max(0, (pkg.crawls - throttle.hits));

        response.set('X-Rate-Sp-Limit', pkg.crawls);
        response.set('X-Rate-Sp-Remaining', remaining);
        response.set('X-Rate-Sp-Reset', timeUntilReset);
        request.throttle = throttle;

        if (throttle.hits < pkg.crawls) {
            return next();
        } else {
            return new Responses(response,"RATE_LIMIT");
        }
    }

    throttle(req, res, next) {
        //before going further we check the limit on this apiKey
        var ip = req.headers['x-forwarded-for'] ||
            req.connection.remoteAddress ||
            req.socket.remoteAddress ||
            req.connection.socket.remoteAddress;

        this.userPkg(req.decoded._doc.pkgId,(pkg) => {
            //console.log("new pkg id", pkg);
            //console.log("key", req.decoded._doc.apiKey);

            // this check is necessary for some clients that set an array of IP addresses
            ip = (ip || '').split(',')[0];


            Throttle
                .findOneAndUpdate({apiKey: req.decoded._doc.apiKey},
                    {$inc: {hits: 1}},
                    {upsert: false})
                .exec((error, throttle) => {
                    if (error) {
                        res.statusCode = 500;
                        return next(error);
                    } else if (!throttle) {
                        //This sets the expiry to 0 and will expire at the createdAt date.
                        let theDate = new Date();
                        theDate.setSeconds(theDate.getSeconds() + pkg.seconds);
                        throttle = new Throttle({
                            createdAt: theDate,
                            apiKey: req.decoded._doc.apiKey
                        });
                        throttle.save((error, throttle) => {
                            if (error) {
                                res.statusCode = 500;
                                //console.log('errror',error);
                                return next(error);
                            } else if (!throttle) {
                                return new Responses(res,"SERVER_ERROR");
                            }

                            this.respondWithThrottle(req, res, next, throttle, pkg);
                        });
                    } else {
                        this.respondWithThrottle(req, res, next, throttle, pkg);
                    }
                });
        });

    };
}