export class Responses {
    res:any;
    response = {
        AUTH_FAILED:{
            code:403,
            message:'Failed to authenticate token.'
        },
        NO_TOKEN: {
            code:403,
            message: 'No token provided'
        },
        INVALID_DATA:{
            code:400,
            message: 'Invalid data'
        },
        SERVER_ERROR:{
            code:500,
            message: 'Server error'
        },
        NO_PROFILE:{
            code:500,
            message: 'Profile config not setup'
        },
        SERVICE_UNAVAILABLE:{
            code:500,
            message: 'Service currently unavailable'
        },
        NO_PACKAGE:{
            code:500,
            message: 'Service currently unavailable'
        },
        NO_PROFILE_STEPS:{
            code:500,
            message: 'Profile steps not setup'
        },
        CRAWL_UNFINISHED:{
            code:500,
            message: 'Crawl did not finish'
        },
        INTERVAL_EXCEEDED:{
            code:429,
            message: 'Too many requests interval exceeded, please try again after X-Rate-Sp-Interval-Reset'
        },
        RATE_LIMIT:{
            code:429,
            message: 'Too many requests limit exceeded, please wait and try after X-Rate-Sp-Reset'
        },
        NO_SP_KEY:{
            code:400,
            message: 'Authentication failed. No x-sp-key provided.'
        },
        NO_USER:{
            code:400,
            message: 'Authentication failed.'
        },
        INVALID_API_KEY:{
            code:400,
            message: 'Authentication failed. Invalid api key.'
        },
        INVALID_BARCODE_LENGTH:{
            code:400,
            message: 'Invalid barcode length'
        },
        INVALID_BARCODE_CHAR:{
            code:400,
            message: 'Invalid barcode characters'
        }
    };
    constructor(res:any,state:string,additionalHeaders?) {
        this.res = res;
        this.sendResponse(this.response[state],additionalHeaders);
    }
    sendResponse(state,additionalHeaders?) {
        this.addHeaders(additionalHeaders).then(() => {
            this.res.statusCode = state.code;
            return this.res.json([
                {message: state.message}
            ]);
        });
    }
    addHeaders(additionalHeaders?) {
        return new Promise((resolve,reject) => {
            if(additionalHeaders) {
                for (var i = 0; i < additionalHeaders.length; i++) {
                    this.res.setHeader(additionalHeaders[i].header, additionalHeaders[i].value);
                }
            }
            resolve();
        });
    }
}