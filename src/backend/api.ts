'use strict';

import {CrawlController} from "./crawlController";
import {app} from '../../src/server';
import {gtinSteps} from './gtin-steps';
import {Throttler} from './throttle';
import {Responses} from './responses';
import {User} from '../+app/models/user.model';
var util = require('util');
var {Router} = require('express');
var Servers = require('../../models/servers.model.js');
var Crawl = require('../../models/crawl.model.js');
var jwt    = require('jsonwebtoken');
var RateLimit = require('express-rate-limit');
var Packages = require('../../models/package.model.js');
var Products = require('../../models/product.model.js');

export function createCrawlApi() {

  var router = Router();

  // route middleware to verify a token
  router.use((req, res, next) => {

    var _self = this;

    // check header or url parameters or post parameters for token
    var token = req.body['x-sp-token'] || req.query['x-sp-token'] || req.headers['x-sp-token'];

    // decode token
    if (token) {

      // verifies secret and checks exp
      jwt.verify(token, app.get('superSecret'), function(err, decoded) {
        if (err) {
            return new Responses(res,"AUTH_FAILED");
        } else {
          // if everything is good, save to request for use in other routes
          req.decoded = decoded;
          next();
        }
      });

    } else {

      // no token provided
        return new Responses(res,"NO_TOKEN");

    }
  });


    // throttle requests
    var doThrottle = function(request, response, next) {
       return new Throttler(request,response,next);
    };

    var getConfig = function(req,res) {
        if(!req.decoded._doc.config) {
            return new Responses(res,"NO_PROFILE");
        }
        return req.decoded._doc.config;
    };

    var getDate = function() {
        let rand = (Math.random() * (2 - 3.900) + 3.900);
        let now = new Date().getTime();
        let timestamp = Number(now) - (rand*20000);
        let datePlus = new Date(timestamp);
        return datePlus;
    };


    //api limits
    let crawlerLimiter = new RateLimit({
        windowMs: 1000, // 24 hour window
        delayAfter: 1, // begin slowing down responses after the first request
        delayMs: 1*1000, // slow down subsequent responses by 3 seconds per request
        max: 5, // start blocking after 5k requests
        message: "Too many requests, please try again after 5 seconds"
    });
    router.use('/price', crawlerLimiter);

    router.route('/price')
    .post(doThrottle,function(req, res:any) {


      // check header or url parameters or post parameters for token
      let crawl = new Crawl(req.body);

      if(!req.body) {
          return new Responses(res,"INVALID_DATA");
      }
      crawl.save(function(err, crawlResult) {
        if(err) {
            return new Responses(res,"SERVER_ERROR");
        } else {
          let config = getConfig(req,res);
          let datePlus = getDate();

            //find IP's available
            Servers.find({
                userId:req.decoded._doc._id,
                status:true

            }, (err, docs) => {
                if(docs.length === 0) {
                    return new Responses(res,"SERVICE_UNAVAILABLE");
                } else {

                    //find available servers
                    Servers.find({
                        userId:req.decoded._doc._id,
                        last_used:{ $lt : datePlus.toISOString() },
                        status:true

                    }, (err, docs) => {
                        //No servers available
                        if(docs.length === 0) {
                            Packages.findOne({_id: req.decoded._doc.pkgId}, function (err, pkgs) {
                                if (err) {
                                    return new Responses(res,"NO_PACKAGE");
                                } else {
                                    return new Responses(res,"INTERVAL_EXCEEDED",[
                                        {header:'X-Rate-Sp-Interval-Reset',
                                        value:((pkgs.servers * 24 * 3600) / pkgs.crawls)/10 * 1000}

                                    ]);
                                }
                            });
                        } else {
                            //Run the crawl
                            let steps = new gtinSteps(config, req.body, docs[0].ip);
                            if (!steps.check()) {
                                return new Responses(res,"NO_PROFILE_STEPS");
                            }
                            steps.valid().then(() => {
                                let crawl = new CrawlController(steps.getSteps());
                                Servers.findOneAndUpdate({ip: docs[0].ip},
                                    {$set: {last_used: new Date()}, $inc: {hits: 1}})
                                    .exec((error, result) => {
                                    });
                                crawl.runSteps().then((priceMatch: any) => {
                                    if (priceMatch.result.err) {
                                        Servers.findOneAndUpdate({ip: docs[0].ip},
                                            {$set: {status: false, msg: priceMatch.result.err}, $inc: {hits: 1}})
                                            .exec((error, result) => {
                                                //console.log(result);
                                            });
                                        return new Responses(res,"SERVER_ERROR");
                                    }
                                    //save all competitor data found
                                    if(priceMatch.full.allBrands.length > 0) {
                                        var product = new Products({
                                            barcode: req.body.identifier,
                                            priceData: priceMatch.full.allBrands,
                                            priceMatch: priceMatch.result,
                                            userId:req.decoded._doc._id,
                                            crawlData:priceMatch.full.html,
                                            crawlId:crawlResult._id,
                                            crawlerIp:docs[0].ip
                                        });
                                        product.save(function(err, productResult) {
                                            if (err) {
                                                return new Responses(productResult, "SERVER_ERROR");
                                            }
                                        });

                                    }
                                    res.status(priceMatch.result.status).send({
                                        success: true,
                                        price: priceMatch.result.price,
                                        seller: priceMatch.result.brand,
                                    });
                                }).catch((error) => {
                                    if (typeof error.err != 'undefined') {
                                        Servers.findOneAndUpdate({ip: docs[0].ip},
                                            {$set: {status: false, msg: error.err}, $inc: {hits: 1}})
                                            .exec((error, result) => {
                                            });
                                    }
                                    return new Responses(res,"CRAWL_UNFINISHED");
                                });
                            }).catch((err) => {
                                return new Responses(res,err);
                            });
                        }
                    });

                }
            });



        }
      });
    });
  router.route('/user')
      .post(function(req, res) {
          User.getOne(req.body).then(c => {
              return res.json(c);
          });
      });
  return router;
}
