var mongoSeed = require('mongo-seed');

mongoSeed.load("localhost",27017, "squareprice", "seeds/crawls.json", "file", function (err) {
   console.log(err);
});

mongoSeed.load("localhost",27017, "squareprice", "seeds/users.json", "file", function (err) {
    console.log(err);
});

mongoSeed.load("localhost",27017, "squareprice", "seeds/packages.json", "file", function (err) {
    console.log(err);
});