var mongoose = require('mongoose');

var serverModel = mongoose.Schema({
    ip: {type:String, required:true},
    userId: mongoose.Schema.Types.ObjectId,
    last_used: { type: Date, default: Date.now },
    status: {type:Boolean, required:true},
    hits: {type:Number, required:true},
    msg: {type:String}
});
serverModel.index({ last_used: 1, userId:1 });
var Servers = mongoose.model('Servers', serverModel);

module.exports = Servers;