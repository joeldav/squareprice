var mongoose = require('mongoose');

var throttleSchema = mongoose.Schema({
    createdAt: {
        type: Date,
        required: true,
        default: Date.now
    },
    apiKey: mongoose.Schema.Types.ObjectId,
    hits: {
        type: Number,
        default: 1,
        required: true
    }
});
throttleSchema.index({ createdAt: 1  }, { expireAfterSeconds: 0 });
var Throttle = mongoose.model('Throttle', throttleSchema);

module.exports = Throttle;
