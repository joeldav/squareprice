var mongoose = require('mongoose');

var crawlSchema = mongoose.Schema({
    startDate:{ type: Date, default: Date.now },
    endDate:{ type: Date },
    status:String,
    userId: mongoose.Schema.Types.ObjectId,
    price:Number,
    identifier:{type:String, required:true},
    priceMatch:Number
});
crawlSchema.index({ startDate: 1,endDate: 1 });
var Crawl = mongoose.model('Crawl', crawlSchema);

module.exports = Crawl;