var mongoose = require('mongoose');

var productModel = mongoose.Schema({
    barcode: {type:String, required:true},
    priceData:{type:mongoose.Schema.Types.Mixed},
    priceMatch:{type:mongoose.Schema.Types.Mixed},
    userId: mongoose.Schema.Types.ObjectId,
    crawlData: {type:mongoose.Schema.Types.Mixed},
    crawlerIp:{type:String, required:true},
    crawlDate: { type: Date, default: Date.now, index: true },
    crawlId: mongoose.Schema.Types.ObjectId,
    status: {type:Boolean}
});
productModel.index({ crawlDate: 1 });
var Products = mongoose.model('Products', productModel);

module.exports = Products;