var mongoose = require('mongoose');

var pkgModel = mongoose.Schema({
    name: {type:String, required:true},
    crawls: {type:Number, required:true},
    seconds: {type:Number, required:true},
    servers: {type:Number, required:true},
    web: {type:mongoose.Schema.Types.Mixed,required:true},
    api: {type:Boolean, required:true},
    method: {type:String, required:true}
});
var Packages = mongoose.model('Packages', pkgModel);

module.exports = Packages;
